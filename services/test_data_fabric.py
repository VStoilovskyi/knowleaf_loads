import time
import random
from faker import Faker
from constants import PASSWORD

fake = Faker()


class UserDataFabric:
    def __init__(self):
        self.firstName = fake.first_name()
        self.lastName = fake.last_name()
        self.email = f'{self.firstName}.{self.lastName}{random.randint(0,1000)}@example.com'
        self.password = PASSWORD


class CompanyDataFabric:
    def __init__(self, token):
        self.company = {
            "name": fake.text(20),
            "title": fake.text(15),
            "licenseState": 'California',
            "city": "Los Angeles",
            "stateAbbreviation": "CA",
            "phone": f'1{str(time.time()).replace(".", "")[-10:]}',
            "zipCode": "90001",
            "companyTypes": [5]
        }
        self.token = token


def get_products_model():
    return [{
        "subTypeId": 1,
        "brandName": fake.text(50),
        "location": "Los Angeles, CA",
        "name": fake.text(75),
        "isNeedEdit": False,
        "thc": 0,
        "cbd": 0,
        "stockStatus": "IN_STOCK",
        "isPublished": True,
        "isSendQuote": True,
        "bid": {
            "price": 77777,
            "sizeUnit": "mL",
            "size": 889
        },
    },
    {
        "subTypeId": 1,
        "brandName": fake.text(50),
        "location": "Los Angeles, CA",
        "name": fake.text(75),
        "isNeedEdit": False,
        "thc": 0,
        "cbd": 0,
        "stockStatus": "IN_STOCK",
        "isPublished": True,
        "isSendQuote": True,
        "bid": {
            "price": 77777,
            "sizeUnit": "mL",
            "size": 889
        }
    }
]
