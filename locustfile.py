import json
from locust import HttpLocust, TaskSet, task, between
from constants import BASE_API_URL
from services import CompanyDataFabric, UserDataFabric, get_products_model


user_data = UserDataFabric()
access_token = ''
register_token = ''


class UserBehaviour(TaskSet):

    def on_start(self):
        self.sign_up()

    def __fill_company_info(self):
        r = self.client.post("/companies", headers={"content-type": "application/json"},
                             data=json.dumps(CompanyDataFabric(register_token).__dict__))
        global access_token
        access_token = r.json()['data']['session']['accessToken']

    def sign_up(self):
        r = self.client.post("/users", headers={"content-type": "application/json"},
                             data=json.dumps(UserDataFabric().__dict__))
        global register_token
        register_token = r.json()['data']['token']
        self.__fill_company_info()

    @task(1)
    def add_products(self):
        global access_token
        headers = {
            "content-type": "application/json",
            "Authorization": f"Bearer {access_token}"
        }
        r = self.client.post('/products', headers=headers, json=get_products_model())

    @task(3)
    def profile(self):
        global access_token
        headers = {
            "content-type": "application/json",
            "Authorization": f"Bearer {access_token}"
        }
        r = self.client.get("/users/me/profile", headers=headers)

    @task(4)
    def search1(self):
        global access_token
        headers = {
            "content-type": "application/json",
            "Authorization": f"Bearer {access_token}"
        }
        self.client.get('/products/search', headers=headers, params={'limit': 10})

    @task(2)
    def search2(self):
        global access_token
        headers = {
            "content-type": "application/json",
            "Authorization": f"Bearer {access_token}"
        }
        self.client.get('/products/search', headers=headers, params={'limit': 10, "offset": 10})

    @task(3)
    def inventory(self):
        global access_token
        headers = {
            "content-type": "application/json",
            "Authorization": f"Bearer {access_token}"
        }
        self.client.get('/products/inventory', headers=headers, params={'limit': 5})


class WebsiteUser(HttpLocust):
    task_set = UserBehaviour
    wait_time = between(0, 1)
    host = BASE_API_URL
